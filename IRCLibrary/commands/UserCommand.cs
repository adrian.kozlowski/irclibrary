﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IRCLibrary.interfaces;

namespace IRCLibrary.commands {
    public class UserCommand :AbstractCommand {

        public string Username { get; set; }
        public string Mode { get; set; }
        public string RealName { get; set; }

        /// <summary>
        ///[user] [mode] [unused] [realname]
        /// 
        /// </summary>
        public UserCommand(string username, string mode, string realName) {
            this.Username = username;
            this.Mode = mode;
            this.RealName = realName;

        }

        public override string execute() {
            return String.Format("USER {0} {1} * {2}\n", Username, Username, RealName);
        }
    }
}
