﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IRCLibrary.interfaces {
    public delegate void ConnectedHandler(EventArgs args); 
    public delegate void LineHandler(string args);
    public delegate void SendCommandHandler(string args);

    public abstract class AbstractConnection : AbstractObject {
        protected string host;
        protected int port;
        public abstract void Connect(string host, int port);
        public abstract void Disconnect();
    }
}
