﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IRCLibrary.interfaces;

namespace IRCLibrary.objects {
    public class Nick : AbstractObject {
        public string Nickname { get; private set; }
        public string Mask { get; private set; }
        public string Host { get; private set; }
        public bool isOperator { get; private set; }
        public bool isAway {get; private set;}
        public bool isInvisible {get; private set;}
        public bool isGlobalOperator {get; private set;}
        public bool isWallops {get; private set;}
        public bool isRestricted{get; private set;}
        public bool isServerNotices {get;private set;}
    }
}
