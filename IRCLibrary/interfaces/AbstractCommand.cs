﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IRCLibrary.interfaces {
    public abstract class AbstractCommand : AbstractObject {
        public abstract string execute();
    }
}
