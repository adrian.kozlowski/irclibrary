﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IRCLibrary.interfaces;

namespace IRCLibrary.objects {
    public class Message : AbstractObject {
        public string Nick { get; set; }
        public string Username { get; set; }
        public string Host { get;  set; }
        public string MessageText { get;  set; }
        public DateTime Date { get;  set; }
        public string Channel { get; set; }
    }
}
