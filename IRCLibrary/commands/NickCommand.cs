﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IRCLibrary.interfaces;

namespace IRCLibrary.commands {
    class NickCommand : AbstractCommand {
        public string Nick { get; set; }

        public override string execute() {
            return String.Format("NICK {0}\n",Nick);
        }
    }
}
