﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Sockets;
using IRCLibrary.interfaces;
using System.Threading;
using IRCLibrary.commands;
using System.Net;

namespace IRCLibrary {
    class IdentService {
        private Stream stream;
        private TcpListener tcpListener;
        private Thread listenThread;

        public IdentService() {
            Console.WriteLine("Ident service starting...");
            tcpListener = new TcpListener(113);
            this.listenThread = new Thread(new ThreadStart(ListenForClients));
            this.listenThread.Start();
        }


        private void ListenForClients() {
            this.tcpListener.Start();
            Console.WriteLine("Ident service started...");
            while (true) {
                //blocks until a client has connected to the server
                TcpClient client = this.tcpListener.AcceptTcpClient();

                //create a thread to handle communication 
                //with connected client
                Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
            }
        }

        private void HandleClientComm(object client) {
            TcpClient tcpClient = (TcpClient)client;
            StreamReader sr = new StreamReader(tcpClient.GetStream());
            string line;
            while (null != (line = sr.ReadLine())) {
                Console.WriteLine(line);
            }
        }
       

    }
}
