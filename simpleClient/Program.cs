﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Simpleclient {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
            //string text = "::my.server.name NOTICE quider :Highest connection count: 2 (2 clients)";
            //Regex r = new Regex("^:(\\S+)\\s(NOTICE)\\s(\\S+)\\s:(.+)");
            //Match m = r.Match(text);
            //if (m.Success) {
            //    for (int i = 0; i < m.Groups.Count; i++) {
            //        Console.WriteLine(i + ": " + m.Groups[i]);
            //    }
            //} else {
            //    Console.WriteLine("nie znalaziono");
            //}
        }
    }
}
