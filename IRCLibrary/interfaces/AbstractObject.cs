﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IRCLibrary.interfaces {
    public abstract class AbstractObject {
        private string name;
        
        public string Name {
            get {
                return name;
            }
            set {
                this.name = value;
            }
        }

        public AbstractObject() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public AbstractObject(string name) {
            this.name = name;
        }
    }
}
