﻿using IRCLibrary.objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace IRCLibrary {
    internal class Helper {

        internal static Message PrefixDispatch(Match m, string prefix, string channel, string text) {
            var message = new Message();
            

            var regexMessagePattern = "^:(.[^\\s]+)!(.[^\\s]+)@(.[^\\s]+)$";
            var regexMessage = new Regex(regexMessagePattern);
            Match match = regexMessage.Match(prefix);
            message = new Message();
            if(match.Success) {
                message.Nick = match.Groups[1].Value;
                message.Username = match.Groups[2].Value;
                message.Host = match.Groups[3].Value;
                message.Channel = channel;
                message.Date = DateTime.Now;
                message.MessageText = text;
            }
            return message;
        }

    }
}
