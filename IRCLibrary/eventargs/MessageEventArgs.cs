﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IRCLibrary.objects;

namespace IRCLibrary.Events.Args {
    public class MessageEventArgs :EventArgs {
        public Message Msg { get; set; }
        public Channel Channel {
            get;
            set;
        }

    }
}
