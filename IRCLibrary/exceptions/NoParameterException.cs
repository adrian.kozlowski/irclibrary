﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IRCLibrary.exceptions {
    public class NoParameterException : Exception{

        public NoParameterException(string parameterName): 
            base(string.Format("Parameter {0} is not initialized properly but it's required.",parameterName)) {
        }
    }
}
