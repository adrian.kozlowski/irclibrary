﻿using IRCLibrary.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IRCLibrary.objects {
    public class ServerObject : AbstractObject {
        public string Text {
            get;
            set;
        }
    }
}
