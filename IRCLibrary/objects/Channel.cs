﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IRCLibrary.interfaces;

namespace IRCLibrary.objects {
    public class Channel : AbstractObject {
        private IList<Nick> nicks;

        public string Topic { get; private set; }
        public IList<Nick> Nicks {
            get {
                return nicks;
            }
        }

        public Channel(string name)
            : base(name) {

        }

        internal void AddNick(Nick nick) {
            nicks.Add(nick);
        }
    }
}
