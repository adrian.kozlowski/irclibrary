﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IRCLibrary.interfaces;

namespace IRCLibrary.commands {
    class JoinCommand : AbstractCommand {
        public string Name { get; set; }

        public override string execute() {
            return string.Format("JOIN #{0}", Name);
        }
    }
}
