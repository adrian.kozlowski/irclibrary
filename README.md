irclibrary
==========
In my intence was to create the most simple orc library ever. You can use events to implement you own irc client. Take a look how easy is setup and only few events:

How to use?
==========
```
try {
  irc = new Irc();
  irc.LineArrived += new IRCLibrary.interfaces.LineHandler(irc_LineArrived);
  irc.MessageArrived += irc_MessageArrived;
  irc.Mode = "0";
  irc.Nick = "yournick";
  irc.RealName = "RealName";
  irc.Username = "yourUsername";
  irc.Connect("localhost",6667);
} catch(Exception ex) {
  MessageBox.Show(ex.Message);
}
```
