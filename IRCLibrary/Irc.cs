﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IRCLibrary.interfaces;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using IRCLibrary.commands;
using IRCLibrary.objects;
using System.Text.RegularExpressions;
using System.Diagnostics;
using IRCLibrary.exceptions;
using IRCLibrary.Events.Args;

namespace IRCLibrary {
    public delegate void IrcEventHandler(MessageEventArgs e);

    public class Irc : AbstractConnection {

        private List<String> _alternateNames;

        public string Nick {
            get;
            set;
        }
        public string Username {
            get;
            set;
        }
        public string Mode {
            get;
            set;
        }
        public string RealName {
            get;
            set;
        }
        public IList<Channel> Channels {
            get;
            private set;
        }
        public List<string> AlternateNames {
            get {
                return _alternateNames;
            }
        }
        public string ServerName {
            get;
            private set;
        }

        public event ConnectedHandler Connected;
        public event LineHandler SendLine;
        public event IrcEventHandler ChannelJoined;
        public event IrcEventHandler MessageArrived;
        public event IrcEventHandler PartedChannel;
        public event IrcEventHandler PartChannel;

        private Stream stream;
        private TcpClient socket;
        private Nick me;
        private Thread readingThread;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        public Irc(string host = "localhost", int port = 6667) {
            this.host = host;
            this.port = port;

            this.InitializeIrc();
        }

        public Irc() {
            this.InitializeIrc();
        }


        #region diagnostic
        private void Irc_Connected(EventArgs args) {
#if DEBUG
            Debug.WriteLine("Połączony");
#endif
        }

        #endregion

        #region Private Methods

        private void InitializeIrc() {
            //IdentService ident = new IdentService();
            Connected += new ConnectedHandler(Irc_Connected);

        }

        /// <summary>
        /// Reading from stream
        /// </summary>
        private void ReadContext() {
            StreamReader sr = new StreamReader(stream);
            string line;
            try {
                Regex regex = null;
                while((line = sr.ReadLine()) != null) {
                    //TODO: Handle sutuation when nick is taken
                    if(line.StartsWith("PING")) {
                        string[] ping = line.Split(new char[] { ':' });
                        string pong = String.Format("PONG :{0}", ping[1]);
                        SendLine(pong);
                    } else {
                        regex = new Regex("((?=^:)(.[^\\s]+) (.[a-zA-Z]+|[0-9]{3,3}) (.[a-z,\\s]+)|(.[a-zA-Z]+|[0-9]{3,3}) (.+ )*):{0,1}(.*)");

                        Match m = regex.Match(line);
#if DEBUG
                        Debug.WriteLine(line);
#endif
                        if(m.Success) {
                            //Groups 3 and 5 are responsible for command 
                            string command = !string.IsNullOrEmpty(m.Groups[3].Value) ? m.Groups[3].Value : (!string.IsNullOrEmpty(m.Groups[5].Value)) ? m.Groups[5].Value : null;
                            switch(command) {
                                case "PRIVMSG": // group 3 and 4 or 5 and 6
                                    {
                                        var prefix = m.Groups[2].Value;
                                        var channel = m.Groups[4].Value;
                                        var text = m.Groups[7].Value;
                                        var message = Helper.PrefixDispatch(m, prefix, channel, text);
                                        if(MessageArrived != null) {
                                            MessageArrived(new MessageEventArgs() {
                                                Msg = message
                                            });
                                        }
                                    }
                                    break;
                                case "JOIN"://channel name in group 7
                                    if(ChannelJoined != null) {
                                        ChannelJoined(new MessageEventArgs() {
                                            Channel = new Channel(m.Groups[7].Value) {
                                                //channel info
                                            },
                                            Msg = new Message() {
                                                //message info
                                            }
                                        });
                                    }
                                    break;
                                case "PART": {
                                        var prefix = m.Groups[2].Value;
                                        var channel = m.Groups[4].Value;
                                        var text = m.Groups[7].Value;
                                        var message = Helper.PrefixDispatch(m, prefix, channel, text);
                                        if(MessageArrived != null) {
                                            MessageArrived(new MessageEventArgs() {
                                                Msg = message
                                            });
                                        }
                                    }
                                    break;
                                case "MODE":
                                    break;
                                case "353": // nicks in channel
                                    break;
                                case "366":
                                case "NOTICE":
                                default:
                                    var msg = new Message();

                                    msg.MessageText = m.Groups[7].Value;
                                    if(MessageArrived != null) {
                                        MessageArrived(new MessageEventArgs() {
                                            Msg = msg
                                        });
                                    }

                                    break;
                            }
                        } else {

                        }
                    }
                }
            } catch(Exception e) {
                throw;
            } finally {
                //sr.Close();
            }
        }

        /// <summary>
        /// Writing to stream
        /// <summary>
        /// <param name="args"></param>
        private void WriteContext(string args) {
            StreamWriter sw = new StreamWriter(stream);
            Console.WriteLine(args);
            sw.WriteLine(args);
            sw.Flush();
        }

        /// <summary>
        /// Establishing server connection. Makes also first validation and check if all required
        /// parameters are set.
        /// </summary>
        private void Connect() {
            SendLine += new LineHandler(WriteContext);

            try {
                socket = new TcpClient(this.host, this.port);
                Connected(new EventArgs());
            } catch(Exception e) {
                throw new IOException(e.Message);
            }
            try {
                stream = socket.GetStream();
                readingThread = new Thread(new ThreadStart(ReadContext));
                readingThread.IsBackground = true;
                readingThread.Name = "reading thread";
                readingThread.Start();

                AbstractCommand uc;
                if(string.IsNullOrEmpty(this.RealName) || string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(this.Mode))
                    throw new NoParameterException("Real name or Username or mode");
                uc = new UserCommand(this.Username, this.Mode, this.RealName);
                SendLine(uc.execute());

                uc = new NickCommand() {
                    Nick = this.Nick
                };
                if(string.IsNullOrEmpty(this.Nick))
                    throw new NoParameterException("Nick");

                SendLine(uc.execute());

#if DEBUG
                uc = new JoinCommand() {
                    Name = "interia"
                };
                SendLine(uc.execute());
#endif

            } catch(Exception ex) {
                throw ex;
            }
        }

        #endregion

        /// <summary>
        /// Adds alternate name to nameset
        /// </summary>
        /// <param name="username">alternate username</param>
        /// <returns></returns>
        public Irc AddAlternateUsername(string username) {
            if(_alternateNames == null) {
                _alternateNames = new List<string>();
            }
            _alternateNames.Add(Name);
            return this;
        }

        #region ircActions

        /// <summary>
        /// Joins channel
        /// </summary>
        /// <param name="channelName"> channel name w/o #</param>
        public void JoinChannel(string channelName) {
            AbstractCommand command = new JoinCommand() {
                Name = channelName
            };
            SendLine(command.execute());
        }

        #endregion

        /// <summary>
        /// Connects to server passed as parameter
        /// </summary>
        /// <param name="host">Server used to connection i.e.: 127.0.0.1</param>
        /// <param name="port">Integer, port used to connection. Most used 6667.</param>
        public override void Connect(string host, int port) {
            this.host = host;
            this.port = port;
            this.Connect();
        }

        /// <summary>
        /// Disconnects socket
        /// </summary>
        public override void Disconnect() {
            socket.Client.Disconnect(true);
        }
    }
}
