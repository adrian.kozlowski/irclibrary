﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IRCLibrary;
using IRCLibrary.Events.Args;

namespace Simpleclient {
    delegate void Invoker(string line);
    public partial class MainForm : Form {
        private Irc irc;

        public MainForm() {
            InitializeComponent();
        }

        private void MainForm_Shown(object sender, EventArgs e) {
            try {
                irc = new Irc();
                irc.MessageArrived += irc_MessageArrived;
                irc.Mode = "0";
                irc.Name = "quider";
                irc.Nick = "Wlevvo";
                irc.RealName = "RealName";
                irc.Username = "Quider";
                irc.Connect("localhost",6667);
            } catch(Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        void irc_MessageArrived(MessageEventArgs e) {
            Invoker i = insertTextToPanel;
            mainTexT.Invoke(i, new object[] { e.Msg.MessageText }); 
        }

        private void insertTextToPanel(string line) {
            this.mainTexT.Text += line + "\n";
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e) {
            if(Keys.Return == e.KeyCode) {
                irc.JoinChannel("interia");
            }
            
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e) {
            irc.Disconnect();
            irc = null;
            Application.Exit();
        }
    }
}
